package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.LIST;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SpringSecurityTestConfig.class)
@AutoConfigureMockMvc
class DemoApplicationTests {
	
	private static final Logger Logger=LogManager.getLogger(DemoApplicationTests.class);
	
	@Autowired
    private MockMvc mockMvc;
	
	@MockBean
    private RewardsService rewardsService;
	
	@MockBean
    private SecurityContext securityContext;
	
	@Autowired
    private ObjectMapper objectMapper;
	
	
	@Test
	public void testGetMapping() throws JsonProcessingException, Exception {
		String token="eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrYXJhbiIsImV4cCI6MTY3Mzk3MjU1NCwiaWF0IjoxNjczOTU0NTU0fQ.ljEMjL0y4fR2nA9eOJII9yTa2GP7hxxZx84NSZjHds-3GbBEACtNsimWsCZFDSG6fSdp6l47HV2A34BjBJd0Dg";
		Logger.info(token);
		Date date = new Date(2021-12-01);
		List<CustomerRewardDetails> customerRewardDetailsList = new ArrayList<>();
		List<RewardDetails> rewardDetailsList =new ArrayList<>();

		RewardDetails rDetails1 = new RewardDetails(1,1,1,0,new Date(2022-01-01));
		RewardDetails rDetails2 = new RewardDetails(2,2,2,250,new Date(2022-02-01));
		RewardDetails rDetails3 = new RewardDetails(3,3,3,450,new Date(2022-03-01));
		RewardDetails rDetails4 = new RewardDetails(4,4,4,650,new Date(2022-01-01));
		RewardDetails rDetails5 = new RewardDetails(5,1,5,850,new Date(2022-02-01));
		RewardDetails rDetails6 = new RewardDetails(6,2,6,1050,new Date(2022-03-01));
		RewardDetails rDetails7 = new RewardDetails(7,3,7,1250,new Date(2022-01-01));
		RewardDetails rDetails8 = new RewardDetails(8,4,8,1450,new Date(2022-02-01));
		RewardDetails rDetails9 = new RewardDetails(9,1,9,1650,new Date(2022-03-01));
		RewardDetails rDetails10 = new RewardDetails(10,2,10,1850,new Date(2022-01-01));
		RewardDetails rDetails11 = new RewardDetails(11,3,11,2050,new Date(2022-02-01));
		RewardDetails rDetails12 = new RewardDetails(12,4,12,2250,new Date(2022-03-01));
		RewardDetails rDetails13 = new RewardDetails(13,4,13,2450,new Date(2022-03-01));

		rewardDetailsList.add(rDetails1);
		rewardDetailsList.add(rDetails2);
		rewardDetailsList.add(rDetails3);
		rewardDetailsList.add(rDetails4);
		rewardDetailsList.add(rDetails5);
		rewardDetailsList.add(rDetails6);
		rewardDetailsList.add(rDetails7);
		rewardDetailsList.add(rDetails8);
		rewardDetailsList.add(rDetails9);
		rewardDetailsList.add(rDetails10);
		rewardDetailsList.add(rDetails11);
		rewardDetailsList.add(rDetails12);
		rewardDetailsList.add(rDetails13);


		CustomerRewardDetails details1 = new CustomerRewardDetails(1,1,0);
		CustomerRewardDetails details2 = new CustomerRewardDetails(2,2,250);
		CustomerRewardDetails details3 = new CustomerRewardDetails(3,3,450);
		CustomerRewardDetails details4 = new CustomerRewardDetails(4,1,650);
		CustomerRewardDetails details5 = new CustomerRewardDetails(1,2,850);
		CustomerRewardDetails details6 = new CustomerRewardDetails(2,3,1050);
		CustomerRewardDetails details7 = new CustomerRewardDetails(3,1,1250);
		CustomerRewardDetails details8 = new CustomerRewardDetails(4,2,1450);
		CustomerRewardDetails details9 = new CustomerRewardDetails(1,3,1650);
		CustomerRewardDetails details10 = new CustomerRewardDetails(2,1,1850);
		CustomerRewardDetails details11 = new CustomerRewardDetails(3,2,2050);
		CustomerRewardDetails details12 = new CustomerRewardDetails(4,3,2250);
		CustomerRewardDetails details13 = new CustomerRewardDetails(4,3,2450);
		customerRewardDetailsList.add(details1);
		customerRewardDetailsList.add(details2);
		customerRewardDetailsList.add(details3);
		customerRewardDetailsList.add(details4);
		customerRewardDetailsList.add(details5);
		customerRewardDetailsList.add(details6);
		customerRewardDetailsList.add(details7);
		customerRewardDetailsList.add(details8);
		customerRewardDetailsList.add(details9);
		customerRewardDetailsList.add(details10);
		customerRewardDetailsList.add(details11);
		customerRewardDetailsList.add(details12);
		customerRewardDetailsList.add(details13);

		Mockito.when(rewardsService.calculateAndGetRewards(Mockito.any(Date.class))).thenReturn(rewardDetailsList);
        Mockito.when(rewardsService.getCustomerRewardDetails(Mockito.anyList(),Mockito
				.any(Date.class))).thenReturn(customerRewardDetailsList);
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/rewards/rewardDetails")
								.header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
								.param("fromDate",date.toString())
								.content(objectMapper.writeValueAsString(date))).andReturn();
		Logger.info(mvcResult.getResponse().getContentAsString());
		assertEquals(200, mvcResult.getResponse().getStatus());
	}

	@Test
	public void testCalculateRewards() throws JsonProcessingException, Exception {
		List<TransactionDetails> transactionDetailsList = new ArrayList<>();
		TransactionDetails td1 = new TransactionDetails(1,1,"stem","root",new Date(2022-01-01),100);
		TransactionDetails td2 = new TransactionDetails(2,2,"stem","root",new Date(2022-02-01),200);
		TransactionDetails td3 = new TransactionDetails(3,3,"stem","root",new Date(2022-03-01),300);
		TransactionDetails td4 = new TransactionDetails(4,4,"stem","root",new Date(2022-01-01),400);
		TransactionDetails td5 = new TransactionDetails(5,1,"stem","root",new Date(2022-02-01),500);
		TransactionDetails td6 = new TransactionDetails(6,2,"stem","root",new Date(2022-03-01),600);
		TransactionDetails td7 = new TransactionDetails(7,3,"stem","root",new Date(2022-01-01),700);
		TransactionDetails td8 = new TransactionDetails(8,4,"stem","root",new Date(2022-02-01),800);
		TransactionDetails td9 = new TransactionDetails(9,1,"stem","root",new Date(2022-03-01),900);
		TransactionDetails td10 = new TransactionDetails(10,2,"stem","root",new Date(2022-01-01),1000);
		TransactionDetails td11 = new TransactionDetails(11,3,"stem","root",new Date(2022-02-01),1100);
		TransactionDetails td12 = new TransactionDetails(12,4,"stem","root",new Date(2022-03-01),1200);
		TransactionDetails td13 = new TransactionDetails(13,4,"stem","root",new Date(2022-03-01),1300);
		transactionDetailsList.add(td1);
		transactionDetailsList.add(td2);
		transactionDetailsList.add(td3);
		transactionDetailsList.add(td4);
		transactionDetailsList.add(td5);
		transactionDetailsList.add(td6);
		transactionDetailsList.add(td7);
		transactionDetailsList.add(td8);
		transactionDetailsList.add(td9);
		transactionDetailsList.add(td10);
		transactionDetailsList.add(td11);
		transactionDetailsList.add(td12);
		transactionDetailsList.add(td13);
		RewardsService rewardsService1 = new RewardsService();
		List<RewardDetails> rewardDetailsList=rewardsService1.calculateRewardPoints(transactionDetailsList);
		Logger.info(" size is "+rewardDetailsList.size());
		for(RewardDetails rd : rewardDetailsList){
			Logger.info(rd.getRewardPoints());
		}
		assertEquals(13,rewardDetailsList.size());
		assertEquals(0,rewardDetailsList.get(0).getRewardPoints());
		assertEquals(250,rewardDetailsList.get(1).getRewardPoints());
		assertEquals(450,rewardDetailsList.get(2).getRewardPoints());
		assertEquals(650,rewardDetailsList.get(3).getRewardPoints());
		assertEquals(850,rewardDetailsList.get(4).getRewardPoints());
		assertEquals(1050,rewardDetailsList.get(5).getRewardPoints());
		assertEquals(1250,rewardDetailsList.get(6).getRewardPoints());
		assertEquals(1450,rewardDetailsList.get(7).getRewardPoints());
		assertEquals(1650,rewardDetailsList.get(8).getRewardPoints());
		assertEquals(1850,rewardDetailsList.get(9).getRewardPoints());
		assertEquals(2050,rewardDetailsList.get(10).getRewardPoints());
		assertEquals(2250,rewardDetailsList.get(11).getRewardPoints());
		assertEquals(2450,rewardDetailsList.get(12).getRewardPoints());
	}






	}
