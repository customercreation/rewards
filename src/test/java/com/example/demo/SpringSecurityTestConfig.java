package com.example.demo;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@TestConfiguration
public class SpringSecurityTestConfig {

	/*
	 * @Bean
	 * 
	 * @Primary public UserDetailsService userDetailsService11() { PasswordEncoder
	 * passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
	 * 
	 * final User.UserBuilder userBuilder =
	 * User.builder().passwordEncoder(passwordEncoder::encode);
	 * List<GrantedAuthority> userAuthorities =
	 * AuthorityUtils.createAuthorityList("ROLE_USER");
	 * 
	 * UserDetails userUser =
	 * userBuilder.username("karan").password("karan").authorities(userAuthorities).
	 * build();
	 * 
	 * return new InMemoryUserDetailsManager(Arrays.asList(userUser)); }
	 */

}
