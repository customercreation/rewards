package com.example.demo.controller;

import com.example.demo.CustomerRewardDetails;
import com.example.demo.Exception.GlobalExceptionHandler;
import com.example.demo.RewardDetails;
import com.example.demo.RewardsService;
import com.example.demo.TransactionDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/rewards")
public class RewardsController {
	
	private static final Logger Logger=LoggerFactory.getLogger(RewardsController.class);
	
	@Autowired
	RewardsService rewardsService;

	@GetMapping(value="/test")
	public String test() {
		return "okay";
	}

	
	@GetMapping(value="/rewardDetails")
	public ResponseEntity<List<Map<Integer,List<CustomerRewardDetails>>>> getRewardDetails(@RequestParam Date fromDate) {
		try {
			Logger.info(" Inside getRewardDetails");
			List<RewardDetails> details = rewardsService.calculateAndGetRewards(fromDate);
			List<CustomerRewardDetails> customerRewardDetails = rewardsService.getCustomerRewardDetails(details,fromDate);
			Logger.info(" size of customerRewardDetails is"+customerRewardDetails.size());
			Map<Integer,List<CustomerRewardDetails>> rewardsByFirstMonth = new HashMap<>();
			Map<Integer,List<CustomerRewardDetails>> rewardsBySecondMonth = new HashMap<>();
			Map<Integer,List<CustomerRewardDetails>> rewardsByThirdMonth = new HashMap<>();
			rewardsByFirstMonth =customerRewardDetails.stream().filter(e -> e.getMonth()==1).collect(Collectors.groupingBy(CustomerRewardDetails::getCustomerId));
			rewardsBySecondMonth =customerRewardDetails.stream().filter(e -> e.getMonth()==2).collect(Collectors.groupingBy(CustomerRewardDetails::getCustomerId));
			rewardsByThirdMonth =customerRewardDetails.stream().filter(e -> e.getMonth()==3).collect(Collectors.groupingBy(CustomerRewardDetails::getCustomerId));

			List<Map<Integer,List<CustomerRewardDetails>>> customerRewardsByMonthAndTotal = new ArrayList<>();
			customerRewardsByMonthAndTotal.add(rewardsByFirstMonth);
			customerRewardsByMonthAndTotal.add(rewardsBySecondMonth);
			customerRewardsByMonthAndTotal.add(rewardsByThirdMonth);

			return new ResponseEntity<>(customerRewardsByMonthAndTotal, HttpStatus.OK);
		    }
		catch (RuntimeException e) {
			Logger.error("Exception in getRewardDetails", e);
				throw new RuntimeException(e);

		}
		    	
		    }
	}

