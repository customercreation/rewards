package com.example.demo;

import com.example.demo.controller.RewardsController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RewardsService {

	private static final org.slf4j.Logger Logger= LoggerFactory.getLogger(RewardsService.class);
	@Autowired
	RewardsRepository rewardsRepository;


	public List<RewardDetails> calculateAndGetRewards(Date fromDate) {
		Logger.info(" Inside calculateAndGetRewards");
		List<TransactionDetails> details = rewardsRepository.findByFromDate(fromDate, sqlDatePlusDays(fromDate,90));
		for(TransactionDetails transactionDetails:details){
			Logger.info("transaction details are" +transactionDetails.getTransactionId()+
					transactionDetails.getDateOfTransaction()+
					transactionDetails.getAmount()+transactionDetails.getCustomerId());
		}
		List<RewardDetails> rewardDetailsList = new ArrayList<>();
		if (details != null) {
			rewardDetailsList = calculateRewardPoints(details);
		}
		return rewardDetailsList;
	}

	public List<RewardDetails> calculateRewardPoints(List<TransactionDetails> transactionDetails){
		List<RewardDetails> rewardDetailsList = new ArrayList<>();
		for (TransactionDetails rd : transactionDetails) {
			RewardDetails rewardDetails = new RewardDetails();
			rewardDetails.setCustomerId(rd.getCustomerId());
			rewardDetails.setTransactionId(rd.getTransactionId());
			rewardDetails.setDateOfTransaction(rd.getDateOfTransaction());
			if (rd.getAmount() <= 100) {
				rewardDetails.setRewardPoints(0);
			} else {
				int sum = 0;
				int twoDollarSum = (rd.amount - 100) * 2;
				sum = twoDollarSum + 50;
				rewardDetails.setRewardPoints(sum);
			}
			rewardDetailsList.add(rewardDetails);
		}
		return rewardDetailsList;
	}


	public List<CustomerRewardDetails> getCustomerRewardDetails(List<RewardDetails> details,Date fromDate) {
		Logger.info(" Inside getCustomerRewardDetails");
		List<CustomerRewardDetails> customerRewardDetails = new ArrayList<>();
		for(RewardDetails rewardDetails : details){
			CustomerRewardDetails crd = new CustomerRewardDetails();
			Logger.info(" The details are "+ rewardDetails.getCustomerId() + "  " + rewardDetails.getRewardPoints() + "  "+ rewardDetails.getDateOfTransaction() + " "+rewardDetails.getTransactionId());
			if(rewardDetails.getDateOfTransaction().after(fromDate) && rewardDetails.getDateOfTransaction().before(sqlDatePlusDays(fromDate,30))){
				crd.setCustomerId(rewardDetails.getCustomerId());
				crd.setMonth(1);
				crd.setRewardPoints(rewardDetails.getRewardPoints());
			}
			else if(rewardDetails.getDateOfTransaction().after(sqlDatePlusDays(fromDate,30)) && rewardDetails.getDateOfTransaction().before(sqlDatePlusDays(fromDate,60))){
				crd.setCustomerId(rewardDetails.getCustomerId());
				crd.setMonth(2);
				crd.setRewardPoints(rewardDetails.getRewardPoints());
			}
			else if(rewardDetails.getDateOfTransaction().after(sqlDatePlusDays(fromDate,60)) && rewardDetails.getDateOfTransaction().before(sqlDatePlusDays(fromDate,90))){
				crd.setCustomerId(rewardDetails.getCustomerId());
				crd.setMonth(3);
				crd.setRewardPoints(rewardDetails.getRewardPoints());
			}
			else {
				continue;
			}
			customerRewardDetails.add(crd);
			Logger.info("CustomerRewardDetails are"+crd.getCustomerId()+" " +crd.getMonth()+" "+crd.getRewardPoints());

		}
		return customerRewardDetails;
	}

	private Date sqlDatePlusDays(Date date,int days){

		return Date.valueOf(date.toLocalDate().plusDays(days));
	}
}
