package com.example.demo.security;

import javax.security.sasl.AuthorizeCallback;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

@Configuration
@EnableWebSecurity
@EnableEncryptableProperties
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfig {
	
	
	
	
	@Autowired
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

	@Autowired
	private UserDetailsService jwtUserDetailsService;

	@Autowired
	private JwtRequestFilter jwtRequestFilter;
	
	@Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

	
	/*
	 * @Bean public InMemoryUserDetailsManager userDetailsService(PasswordEncoder
	 * passwordEncoder) { UserDetails user = User.withUsername("karan")
	 * .password(passwordEncoder.encode("karan")) .roles("USER") .build();
	 * 
	 * 
	 * 
	 * return new InMemoryUserDetailsManager(user); }
	 */
	
	
	  @Bean 
	  public SecurityFilterChain filterChain(HttpSecurity http) throws  Exception {
		
		  //http.csrf().disable().httpBasic().and().authorizeRequests().anyRequest().authenticated();
		  //http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		  return http.cors().and()
	                .csrf().disable()
	                .authorizeHttpRequests()
	                        //.antMatchers( "/authenticate","/customer/*").permitAll()
				  .antMatchers( "/authenticate").permitAll().anyRequest().authenticated().and()
	                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
	                .addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class)
	                .build();
		  //return http.build(); 
		 }
	 
	
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
	

}
