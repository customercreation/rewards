package com.example.demo;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name="transaction_details")
public class TransactionDetails implements Serializable {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	int transactionId;
	@Column(name="customer_id")
	int customerId;
	@Column(name="first_name")
	String firstName;
	@Column(name="last_name")
	String lastName;
	@Column(name="transaction_date")
	Date dateOfTransaction;
	@Column(name="amount")
	int amount;

	public TransactionDetails() {}

	public TransactionDetails(int transactionId, int customerId,  String firstName, String lastName, Date dateOfTransaction, int amount) {
		this.transactionId = transactionId;
		this.customerId= customerId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfTransaction = dateOfTransaction;
		this.amount = amount;
	}


	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDateOfTransaction() {
		return dateOfTransaction;
	}

	public void setDateOfTransaction(Date dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}
