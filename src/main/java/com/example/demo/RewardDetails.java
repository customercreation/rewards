package com.example.demo;

import javax.persistence.*;
import java.sql.Date;

public class RewardDetails {


	int customerId;
	int transactionId;
	int rewardPoints;

	Date dateOfTransaction;
	public RewardDetails() {}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public int getRewardPoints() {
		return rewardPoints;
	}

	public void setRewardPoints(int rewardPoints) {
		this.rewardPoints = rewardPoints;
	}

	public Date getDateOfTransaction() {
		return dateOfTransaction;
	}

	public void setDateOfTransaction(Date dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}

	public RewardDetails(int rewardId, int customerId, int transactionId, int rewardPoints, Date dateOfTransaction) {
		this.customerId = customerId;
		this.transactionId = transactionId;
		this.rewardPoints = rewardPoints;
		this.dateOfTransaction=dateOfTransaction;
	}


}
