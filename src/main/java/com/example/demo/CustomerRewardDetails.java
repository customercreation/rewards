package com.example.demo;

import java.util.Date;

public class CustomerRewardDetails {


	int customerId;
	int month;
	int rewardPoints;

	public CustomerRewardDetails() {}

	public CustomerRewardDetails(int customerId, int month, int rewardPoints) {
		this.customerId = customerId;
		this.month = month;
		this.rewardPoints = rewardPoints;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getRewardPoints() {
		return rewardPoints;
	}

	public void setRewardPoints(int rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
}
