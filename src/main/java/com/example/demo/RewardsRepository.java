package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface RewardsRepository extends JpaRepository<TransactionDetails,Long>{

	List<TransactionDetails> findByFirstName(String firstName);
	@Query(value = "SELECT id,customer_id,first_name,last_name,transaction_date,amount from transaction_details where transaction_date>=?1 and transaction_date<=?2 ",nativeQuery = true)
	List<TransactionDetails> findByFromDate(Date fromDate, Date toDate);
}
